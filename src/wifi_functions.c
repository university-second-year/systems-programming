/**wififunction.c***********************************************************************

  FileName    [wifi_functions.c]

  Synopsis    [code]

  Description [in this .C file we can find the code for each of the functions ]

  SeeAlso     [
				main.c
				wififunctions.h
								]

  Author      [Paula Madrid Alonso, Adrián Brenes MArtinez, Cristina Bolado Saá and Marta García Canora.]

  Copyright   [Copyright (c) 2012 Carlos III University of Madrid
  All rights reserved.

  Permission is hereby granted, without written agreement and without license
  or royalty fees, to use, copy, modify, and distribute this software and its
  documentation for any purpose, provided that the above copyright notice and
  the following two paragraphs appear in all copies of this software.

  IN NO EVENT SHALL THE CARLOS III UNIVERSITY OF MADRID BE LIABLE TO ANY PARTY
  FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING
  OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF THE CARLOS III
  UNIVERSITY OF MADRID HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

  THE CARLOS III UNIVERSITY OF MADRID SPECIFICALLY DISCLAIMS ANY WARRANTIES,
  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
  FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN
  "AS IS" BASIS, AND CARLOS III UNIVERSITY OF MADRID HAS NO OBLIGATION TO
  PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.]

******************************************************************************/

#include "wifi_functions.h"

/*---------------------------------------------------------------------------*/
/* Constant declarations                                                     */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/* Type declarations                                                         */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/* Structure declarations                                                    */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/* Variable declarations                                                     */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/* Macro declarations                                                        */
/*---------------------------------------------------------------------------*/
/**Macro***********************************************************************

  Synopsis     [#define]

  Description  [we used them since for some of the sizes]

  SideEffects  [none. we used #define so that there is less margin of error]

  SeeAlso            [
						main.c
						wifi_functions.h
											]

******************************************************************************/

#define F_MAX 17
#define S_MAX 3

/*---------------------------------------------------------------------------*/
/* Static function prototypes                                                */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/* Definition of functions                                                   */
/*---------------------------------------------------------------------------*/
/**Functions********************************************************************

  Synopsis           [WFICOLLECTOR_QUIT [1]]

  Description        [offers the possibility of quiting the program]

  Parameters         [char yesorno]

  SeeAlso            [
						main.c
						wifi_functions.h
											]

******************************************************************************/

//WFICOLLECTOR_QUIT [1]
void wificollector_quit(char yesorno[1]) {
  printf("\nAre you sure you want to exit? [y/N]: ");
  int ctrld = scanf("%s", yesorno);
  clearerr(stdin);
  if(tolower(yesorno[0]) == 'y')exit(0);
  else if(tolower(yesorno[0])!='n'){
    while( tolower(yesorno[0])!='n' && ctrld!=EOF){
      printf("\nPlease, enter a valid option [y/N]: ");
      ctrld=scanf("%s",yesorno);
      clearerr(stdin);
      if(tolower(yesorno[0]) == 'y')exit(0);
    }
  }
}

/**Functions********************************************************************

  Synopsis           [WIFICOLLECTOR_COLLECT [2]]

  Description        [reads and collects the information of a cell]

  Parameters         [int ncell, struct access_point *first]

  SeeAlso            [
						main.c
						wifi_functions.h
											]

******************************************************************************/

//WIFICOLLECTOR_COLLECT [2]

struct access_point* wificollector_collect(int ncell, struct access_point *first){
  //open file
  FILE* file;
  char f[F_MAX] = "cells/info_cell_";
  char ncellstr[S_MAX];
  sprintf(ncellstr,"%d",ncell);
  strcat(f, ncellstr);
  strcat( f,".txt");
  file=fopen(f,"r");

  size_t bytes_number = 0;
  int line = 0;

  //vars to create a node
  int cell_identifier2;
  char* address2;
  char* essid2;
  char* mode2;
  int channel2;
  char* encryption_key2;
  int quality2;

  //vars tokens
  char* token;
  char *aux = (char*)malloc(sizeof(char)*30);

  if (!file) {
    fprintf(stderr, "Error opening file \n");
    exit(0);
  }else{
    while(!feof(file)){
      int count = 0;
      char *string = (char*)malloc(sizeof(char)*30);
      if(line==1){
        bytes_number=getline(&string,&bytes_number, file);
        string[strlen(string)-1]='\0';
        if(bytes_number!=EOF){
        //printf("line: %d -> %s\n", line, string);
          token=strtok(string," ");
          while( token != NULL ) {
            count++;
            if (count==2){
              count=0;
              aux=token;
            }
            token = strtok(NULL, " ");
          }
          cell_identifier2 = atoi(aux);
      }
    }
      if(line==2){
        getline(&string,&bytes_number, file);
        string[strlen(string)-1]='\0';
        //printf("line: %d -> %s\n", line, string);
        token=strtok(string," ");
         while( token != NULL ) {
          count++;
          if (count==2){
            aux=token;
            count=0;
          }
          token = strtok(NULL, " ");
        }
       address2 = aux;

      }
      if(line==3){
        getline(&string,&bytes_number, file);
        string[strlen(string)-1]='\0';
        //printf("line: %d -> %s\n", line, string);
        token=strtok(string,":");
        while( token != NULL ) {
          count++;
          if (count==2){
            aux=token;
            count=0;
          }
          token = strtok(NULL, ":");
        }
        essid2 = aux;
        }

      if(line==4){
        getline(&string,&bytes_number, file);
        string[strlen(string)-1]='\0';
        //printf("line: %d -> %s\n", line, string);
        token=strtok(string,":");
        while( token != NULL ) {
          count++;
          if (count==2){
            aux=token;
            count=0;
          }
          token = strtok(NULL, ":");
        } mode2 = aux;
      }

      if(line==5){
        getline(&string,&bytes_number, file);
        string[strlen(string)-1]='\0';
        //printf("line: %d -> %s\n", line, string);
        token=strtok(string,":");
        while( token != NULL ) {
          count++;
          if (count==2){
            aux=token;
            count=0;
          }
          token = strtok(NULL, ":");
        }
        channel2 = atoi(aux);
      }

      if(line==6){
        getline(&string,&bytes_number, file);
        string[strlen(string)-1]='\0';
        //printf("line: %d -> %s\n", line, string);
        token=strtok(string,":");
        while( token != NULL ) {
          count++;
          if (count==2){
            aux=token;
            count=0;
          }
          token = strtok(NULL, ":");
        } encryption_key2 = aux;
      }

      if(line==7){
        getline(&string,&bytes_number, file);
        string[strlen(string)-1]='\0';
        //printf("line: %d -> %s\n", line, string);
        token=strtok(string,"=/");
        while( token != NULL ) {
          count++;
          if (count==2){
            aux=token;
            count=0;
          }
          token = strtok(NULL, "=/");
        } quality2 = atoi(aux);
      }

      if(line==8){
        getline(&string,&bytes_number, file);
        string[strlen(string)-1]='\0';
        //printf("line: %d -> %s\n", line, string);
      }
      if(line==9){
      getline(&string,&bytes_number, file);
      string[strlen(string)-1]='\0';
      //printf("line: %d -> %s\n", line, string);
      }

      if (line==10){
        create_ap(cell_identifier2,address2,essid2,mode2,channel2,encryption_key2,quality2,&first);
        line=0;
      }
      line++;
    }
  }
  fclose(file);
  return first;
}

/**Functions********************************************************************

  Synopsis           [WIFICOLLECTOR_SHOW_DATA_ONE [3]]

  Description        [the program shows you the essids chosen by the user]

  Parameters         [struct access_point *first]

  SeeAlso            [
						main.c
						wifi_functions.h
											]

******************************************************************************/

//WIFICOLLECTOR_SHOW_DATA_ONE [3]

void wificollector_show_data_one(struct access_point *first){
  char* essid;
  essid=(char*)malloc(sizeof(char));
  printf("\nIndicate the ESSID(use \"\")" );
  scanf("%s",essid);
  clearerr(stdin);
  struct access_point *auxstr = (struct access_point*)malloc(sizeof(struct access_point));
  auxstr = first;
  if(auxstr){
    while(auxstr != NULL){
      if(strcmp(auxstr->Essid,essid)==0){
        print_ap(auxstr);
      }auxstr=auxstr->next_ap;
    }
    free(essid);
    free(auxstr);
  }else{printf("There are no access points, you should add them in option 2\n");}
}

/**Functions********************************************************************

  Synopsis           [WIFICOLLECTOR_SELECT_BEST [4]]

  Description        [the program shows detailed information of the network with the best quality]

  Parameters         [struct access_point *first]

  SeeAlso            [
						main.c
						wifi_functions.h
											]

******************************************************************************/

//WIFICOLLECTOR_SELECT_BEST [4]

void wificollector_select_best(struct access_point *first){
  if(first){
  struct access_point* best = (struct access_point*)malloc(sizeof(struct access_point));
  struct access_point *auxstr = (struct access_point*)malloc(sizeof(struct access_point));
  auxstr=first;
  best=first;
  while(auxstr!=NULL){
    if(auxstr->quality>best->quality){
      best=auxstr;
    }
    auxstr=auxstr->next_ap;
  }
  print_ap(best);
  free(best);
  free(auxstr);
}else {printf("\nThere are no access points, you should add them in option 2\n");}
}

/**Functions********************************************************************

  Synopsis           [WIFICOLLECTOR_DELETE_NET [5]]

  Description        [select and delete an essid]

  Parameters         [struct access_point *first]

  SeeAlso            [
						main.c
						wifi_functions.h
											]

******************************************************************************/

//WIFICOLLECTOR_DELETE_NET [5]

struct access_point *wificollector_delete_net(struct access_point* first){
  if(first){char* essid;
  essid=(char*)malloc(sizeof(char));
  printf("\nIndicate the ESSID(use \"\"):" );
  scanf("%s",essid);
  clearerr(stdin);
  struct access_point *prev =(struct access_point*)malloc(sizeof(struct access_point));
  prev=first;
  struct access_point *auxstr = first;
  if(auxstr!=NULL && strcmp(auxstr->Essid,essid)==0){
    first=auxstr->next_ap;
  }
  while(auxstr!=NULL && strcmp(auxstr->Essid,essid)!=0){
    prev=auxstr;
    auxstr=auxstr->next_ap;
  }
  if (auxstr==NULL){ return first;}
    prev->next_ap=auxstr->next_ap;
    free(essid);
    free(auxstr);
    free(prev);}else{printf("\nThere are no access points, you should add them in option 2\n");}
  return first;
}

/**Functions********************************************************************

  Synopsis           [FUNCTION CREATE NODE]

  Parameters         [int cell_identifier,char *adress,char *Essid,char* mode, int channel,char* encryption_key, int quality, struct access_point **ptr]

  SeeAlso            [
						main.c
						wifi_functions.h
											]

******************************************************************************/

//FUNCTION CREATE NODE

void create_ap(int cell_identifier,char *adress,char *Essid,char* mode, int channel,char* encryption_key, int quality, struct access_point **ptr){
  struct access_point* aux;
  aux=(struct access_point*)malloc(sizeof(struct access_point));
  aux->cell_identifier=cell_identifier;
  aux->address=adress;
  aux->Essid=Essid;
  aux->mode=mode;
  aux->channel=channel;
  aux->encryption_key=encryption_key;
  aux->quality=quality;
  aux->next_ap=*ptr;
  *ptr=aux;
}

/**Functions********************************************************************

  Synopsis           [FUNCTION PRINT NODE]

  Parameters         [struct access_point* ap]

  SeeAlso            [
						main.c
						wifi_functions.h
											]

******************************************************************************/

//FUNCTION PRINT NODE
void print_ap(struct access_point* ap){
  printf("\nCell %d \nAdress: %s \nESSID: %s \nMode: %s \nChannel: %d\nEncryption key: %s \nQuality: %d/70 \n", ap->cell_identifier, ap->address, ap->Essid, ap->mode, ap->channel, ap->encryption_key, ap->quality);
}

/*---------------------------------------------------------------------------*/
/* Definition of static functions                                            */
/*---------------------------------------------------------------------------*/

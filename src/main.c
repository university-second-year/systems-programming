/**main.c***********************************************************************

  FileName    [main.c]

  Synopsis    [Design and implementation of an application that collects data about the wifi networks detected by a mobile device. ]

  Description [an application to collect information about the Wifi networks detected with a mobile device while used in open spaces. It scans the available Wifi networks and stores information about their features while moving through places and in vertain times.]

  SeeAlso     [
		wififunctions.h
		wififunctions.c
				]

  Author      [Paula Madrid Alonso, Adrián Brenes MArtinez, Cristina Bolado Saá and Marta García Canora.]

  Copyright   [Copyright (c) 2012 Carlos III University of Madrid
  All rights reserved.

  Permission is hereby granted, without written agreement and without license
  or royalty fees, to use, copy, modify, and distribute this software and its
  documentation for any purpose, provided that the above copyright notice and
  the following two paragraphs appear in all copies of this software.

  IN NO EVENT SHALL THE CARLOS III UNIVERSITY OF MADRID BE LIABLE TO ANY PARTY 
  FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING 
  OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF THE CARLOS III
  UNIVERSITY OF MADRID HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

  THE CARLOS III UNIVERSITY OF MADRID SPECIFICALLY DISCLAIMS ANY WARRANTIES,
  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
  FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN
  "AS IS" BASIS, AND CARLOS III UNIVERSITY OF MADRID HAS NO OBLIGATION TO 
  PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.]

******************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "wifi_functions.h"


/*---------------------------------------------------------------------------*/
/* Constant declarations                                                     */
/*---------------------------------------------------------------------------*/



/*---------------------------------------------------------------------------*/
/* Type declarations                                                         */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/* Structure declarations                                                    */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/* Variable declarations                                                     */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/* Macro declarations                                                        */
/*---------------------------------------------------------------------------*/

/**Macro***********************************************************************

  Synopsis     [#define]

  Description  [we used them since for some of the sizes]

  SideEffects  [none. we used #define so that there is less margin of error]

  SeeAlso            [
						wifi_functions.c
						wifi_functions.h
											]

******************************************************************************/

#define YESORNO_MAX 1



/*---------------------------------------------------------------------------*/
/* Static function prototypes                                                */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/* Definition of functions                                                   */
/*---------------------------------------------------------------------------*/
/**Functions********************************************************************

  Synopsis           [main]

  Description        [it is where the execution of the program is started]

  SeeAlso            [
						main.c
						wifi_functions.c
						wifi_functions.h
											]

******************************************************************************/

int main() {
  int opt;
  int ncell;
  int eof;
  char yesorno[YESORNO_MAX];

  struct access_point *first;
  first=NULL;

  while (eof!=EOF){
    printf("\n[ 1 ] wificollector_quit\n[ 2 ] wificollector_collect\n[ 3 ] wificollector_show_data_one_network\n[ 4 ] wificollector_select_best\n[ 5 ] wificollector_delete_net\n[ 6 ] wificollector_sort\n[ 7 ] wificollector_export\n[ 8 ] wificollector_import\n");
    printf("\n Option: ");
    eof = scanf("%d",&opt);
    if(opt == 1){
      wificollector_quit(yesorno) ;
    }else if(opt == 2){
      yesorno[0] = 'y';
      int ctrld=0;
      while(yesorno[0]=='y' && ctrld!=EOF){
        printf("\nWhat cell do you want to collect? (1 - 21): ");
        ctrld = scanf("%d",&ncell);
        clearerr(stdin);
        while((ncell<1 || ncell>21)&&(ctrld!=EOF)){
          printf("\nPlease, enter a valid number: ");
          ctrld=scanf("%d", &ncell );
          clearerr(stdin);
        }
        if(ctrld!=EOF){first = wificollector_collect(ncell,first);
        printf("\nCell collected.");
        printf("\n\nDo you want to add another access point? [y/N]: ");
        ctrld = scanf("%s", yesorno);
        clearerr(stdin);
        }
      }
    }else if(opt == 3){
      wificollector_show_data_one(first);
    }else if(opt == 4){
      wificollector_select_best(first);
    }
    else if(opt == 5){
      first=wificollector_delete_net(first);
    }else if (opt == 6 || opt == 7 || opt == 8){printf("\nThis option is not ready.\n");}
  }
  return 0;
}

/*---------------------------------------------------------------------------*/
/* Definition of static functions                                            */
/*---------------------------------------------------------------------------*/


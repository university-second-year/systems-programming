/**wififunctions.h*****************************************************************

  FileName    [wifi_functions.h]

  Synopsis    [declaration of the functions]

  Description [File .h is like a "bridge" to the main where we can find all the functions names]

  SeeAlso     [wififunctions.c]

  Author      [Paula Madrid Alonso, Adrián Brenes MArtinez, Cristina Bolado Saá and Marta García Canora.]

  Copyright   [Copyright (c) 2012 Carlos III University of Madrid
  All rights reserved.

  Permission is hereby granted, without written agreement and without license
  or royalty fees, to use, copy, modify, and distribute this software and its
  documentation for any purpose, provided that the above copyright notice and
  the following two paragraphs appear in all copies of this software.

  IN NO EVENT SHALL THE CARLOS III UNIVERSITY OF MADRID BE LIABLE TO ANY PARTY
  FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING
  OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF CARLOS III
  UNIVERSITY OF MADRID HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

  CARLOS III UNIVERSITY OF MADRID SPECIFICALLY DISCLAIMS ANY WARRANTIES,
  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
  FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN
  "AS IS" BASIS, AND CARLOS III UNIVERSITY OF MADRID HAS NO OBLIGATION TO
  PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.]

******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>



/*---------------------------------------------------------------------------*/
/* Constant declarations                                                     */
/*---------------------------------------------------------------------------*/



/*---------------------------------------------------------------------------*/
/* Type declarations                                                         */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/* Structure declarations                                                    */
/*---------------------------------------------------------------------------*/
/**Struct**********************************************************************

  Synopsis    [acces point]

  Description [it contains all the information from each cell]

  SeeAlso            [
						main.c
						wifi_functions.c
											]

******************************************************************************/

struct access_point{
  int cell_identifier ;
  char *address;
  char *Essid ;
  char *mode;
  int channel;
  char *encryption_key;
  int quality;
  struct access_point* next_ap;
};

/*---------------------------------------------------------------------------*/
/* Variable declarations                                                     */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/* Macro declarations                                                        */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/* Function prototypes                                                       */
/*---------------------------------------------------------------------------*/
/**Functions********************************************************************

  Synopsis           [functions void]

  Description        [to see what does each function do read "see also"]

  Parameters         [the ones in theparenthesis]  

  SeeAlso            [
						main.c
						wifi_functions.c
											]

******************************************************************************/


void create_ap(int cell_identifier,char *adress,char *Essid,char* mode, int channel,char* encryption_key, int quality, struct access_point** ptr);
void print_ap(struct access_point* ap);
void wificollector_quit(char yesorno[1]);
struct access_point* wificollector_collect (int ncell, struct access_point *first );
void wificollector_show_data_one(struct access_point *first);
void wificollector_select_best(struct access_point *first);
struct access_point *wificollector_delete_net(struct access_point* first);
void free_ap(struct access_point *auxstr);


